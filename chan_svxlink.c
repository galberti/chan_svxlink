#include <asterisk/asterisk.h>
#include <asterisk/channel.h>
#include <asterisk/causes.h>
#include <asterisk/config.h>
#include <asterisk/dsp.h>
#include <asterisk/format_cache.h>
#include <asterisk/format_compatibility.h>
#include <asterisk/lock.h>
#include <asterisk/logger.h>
#include <asterisk/module.h>
#include <asterisk/network.h>
#include <asterisk/pbx.h>
#include <asterisk/utils.h>

#include <gcrypt.h>

#define AST_MODULE "chan_svxlink"

/* svxlink network helpers */

#define TYPE_HEARTBEAT  1
#define TYPE_PROTOVER   5
#define TYPE_AUTHCHAL   10
#define TYPE_AUTHRESP   11
#define TYPE_AUTHOK     12
#define TYPE_ERROR      13
#define TYPE_SERVINFO   100
#define TYPE_NODELIST   101
#define TYPE_NODEJOIN   102
#define TYPE_NODELEFT   103
#define TYPE_TALKSTART  104
#define TYPE_TALKSTOP   105

#define TYPE_AUDIO      101
#define TYPE_FLUSH      102
#define TYPE_FLUSHED    103

#define MAX_ADDR_LEN    80
#define MAX_CALL_LEN    12
#define MAX_PASS_LEN    80
typedef struct peer {
    char addr[MAX_ADDR_LEN];
    in_port_t port;
    char callsign[MAX_CALL_LEN];
    char password[MAX_PASS_LEN];

    pthread_t admin_thread;
    pthread_t voice_thread;
    int admin_sock;
    int voice_sock;
    uint16_t count;         /* for the transmitted packets */
    uint32_t client_id;

    struct ast_channel *channel;
    unsigned char busy;
    unsigned char tx;
    struct ast_dsp *dsp;
    ast_mutex_t lock;
} peer_t;

#define MAX_PEERS 10
static peer_t peers[MAX_PEERS];
static int num_peers;

#define UNPACK_SHORT(b) ntohs(*(uint16_t*)b)
#define UNPACK_INT(b) ntohl(*(uint32_t*)b);

static int pack_short(unsigned char *buf, uint16_t n){
    uint16_t tmp;
    tmp = htons(n);
    memcpy(buf, &tmp, 2);
    return 2;
}

static int pack_int(unsigned char *buf, uint32_t n){
    uint32_t tmp;
    tmp = htonl(n);
    memcpy(buf, &tmp, 4);
    return 4;
}

static int pack_string(unsigned char *buf, char *string){
    uint16_t tmp;
    tmp = htons(strlen(string));
    memcpy(buf, &tmp, 2);
    memcpy(&buf[2], string, strlen(string));
    return 2+strlen(string);
}

static int send_proto_version(int sock){
    // this is hardcoded, not a big deal
    // all data is network byte order (big endian)
    // len is 6 (4B), ptype is 5, maj version is 1, min version is 0 (2B)
    unsigned char packet[] = "\x00\x00\x00\x06\x00\x05\x00\x01\x00\x00";
    send(sock, packet, 10, 0);
    return 0;
}

static int admin_send_heartbeat(int sock){
    // hardcoded
    unsigned char packet[] = "\x00\x00\x00\x02\x00\x01";
    send(sock, packet, 6, 0);
    return 0;
}

static int voice_send_heartbeat(peer_t *svx_s){
    unsigned char packet[6];
    int i = 0;
    i += pack_short(&packet[i], 1);
    i += pack_short(&packet[i], svx_s->client_id);
    i += pack_short(&packet[i], svx_s->count);
    svx_s->count++;
    send(svx_s->voice_sock, packet, i, 0);
    return 0;
}

static int send_audio_frame(peer_t *p, struct ast_frame *f){
    unsigned char packet[1024];
    int i = 0;
    i += pack_short(&packet[i], TYPE_AUDIO);
    i += pack_short(&packet[i], p->client_id);
    i += pack_short(&packet[i], p->count);
    p->count++;
    i += pack_short(&packet[i], f->datalen);
    memcpy(&packet[i], f->data.ptr, f->datalen);
    send(p->voice_sock, packet, i+f->datalen, 0);
    return 0;
}

static int send_audio_flush(peer_t *p){
    unsigned char packet[80];
    int i = 0;
    i += pack_short(&packet[i], TYPE_FLUSH);
    i += pack_short(&packet[i], p->client_id);
    i += pack_short(&packet[i], p->count);
    p->count++;
    send(p->voice_sock, packet, i, 0);
    return 0;
}

static int send_talk_startstop(peer_t *p, uint16_t startstop){
    unsigned char packet[80];
    int i = 0;
    i += pack_int(&packet[i], 4+strlen(p->callsign));
    i += pack_short(&packet[i], startstop);
    i += pack_string(&packet[i], p->callsign);
    send(p->admin_sock, packet, i, 0);
    return 0;
}

static int send_talk_start(peer_t *p){
    return send_talk_startstop(p, TYPE_TALKSTART);
}

static int send_talk_stop(peer_t *p){
    return send_talk_startstop(p, TYPE_TALKSTOP);
}

#define ALGO GCRY_MD_SHA1
#define DIGEST_LEN 20
#define CHALLENGE_LEN 20
static int compute_digest(unsigned char *challenge, char *passw, int passw_len,
    unsigned char *digest){
    gcry_md_hd_t hd = { 0 };
    unsigned char *digest_ptr = NULL;
    gcry_error_t err = gcry_md_open(&hd, ALGO, GCRY_MD_FLAG_HMAC);
    if(err){
        goto error;
    }
    err = gcry_md_setkey(hd, passw, passw_len);
    if(err){
        goto error;
    }
    gcry_md_write(hd, challenge, CHALLENGE_LEN);
    digest_ptr = gcry_md_read(hd, 0);
    memcpy(digest, digest_ptr, DIGEST_LEN);
    gcry_md_close(hd);
    return 0;

    error:
        gcry_md_close(hd);
        return -1;
}

static int print_packet(int level, const char *file, int line,
    const char *function, unsigned char *buf, int len){
    int i;
    char out[1024];
    out[0] = '\0';

    ast_log(level, file, line, function, "len %d\n", len);
    // cut to 300 bytes
    if(len > 300){
        len = 300;
    }
    for(i = 0; i < len; i++){
        if((i % 16) == 0){
            sprintf(&out[strlen(out)], "\n");
        }
        sprintf(&out[strlen(out)], "%02x ", buf[i]);
    }
    ast_log(level, file, line, function, "%s\n", out);
    return 0;
}

static int voice_unpack(unsigned char *buf, int len, peer_t *svx_s){
    uint16_t type;
    unsigned char out[1024];

    if(len < 2){
        return -1;
    }
    print_packet(AST_LOG_DEBUG, buf, len);
    type = UNPACK_SHORT(buf);
    if(type == TYPE_HEARTBEAT){
        ast_log(AST_LOG_DEBUG, "TYPE_HEARTBEAT\n");
    }else if(type == TYPE_AUDIO){
        struct ast_frame f = { AST_FRAME_VOICE };
        uint16_t length;
        uint16_t seq;
        if(len < 8){
            return -1;
        }
        seq = UNPACK_SHORT(&buf[4]);
        length = UNPACK_SHORT(&buf[6]);
        if(len < length + 8){
            return -1;
        }

        ast_log(AST_LOG_DEBUG, "TYPE_AUDIO len %d seq %d\n", length, seq);
        f.subclass.integer = 0;
        f.subclass.format = ast_format_opus;
        f.datalen = length;
        // FIXME samples per frame?
        f.samples = 1;
        f.data.ptr = &buf[8];
        f.offset = 0;
        f.src = "svxlink";
        f.mallocd = 0;
        f.delivery.tv_sec = 0;
        f.delivery.tv_usec = 0;

        //ast_mutex_lock(&(svx_s->lock));
        if(svx_s->channel){
            //ast_channel_lock(svx_s->channel);
            ast_queue_frame(svx_s->channel, &f);
/*
[Jan 26 11:34:56] WARNING[26775]: dsp.c:1512 ast_dsp_process: Inband DTMF is not supported on codec opus. Use RFC2833
            if(svx_s->dsp){
                struct ast_frame *tf;
                tf = ast_dsp_process(svx_s->channel, svx_s->dsp, &f);
                if(tf && tf->frametype == AST_FRAME_DTMF){
                    ast_log(AST_LOG_NOTICE, "Detected DTMF %c\n",
                        tf->subclass.integer);
                }
                //int ast_dsp_getdigits(struct ast_dsp *dsp, char *b, int max);
                //void ast_dsp_digitreset(struct ast_dsp *dsp);
            }
*/
        }
        //ast_mutex_unlock(&(svx_s->lock));
        //f.samples = FRAME_SIZE;
        /*
        TYPE_AUDIO
        print_packet: 00 65 00 e6 01 2e 00 2c 28 84 ad c5 53 ea 82 6a 6a cc 79 94 d1 eb 4f bb 46 d6 a0 93 71 94 8d 65 c7 27 25 fe d0 53 45 bc d0 c8 4e 8b 0c 27 af 7d ac e2 b2 98
        print_packet: 00 65 00 e6 01 2f 00 11 28 3d 4e 00 45 5d 7d 52 35 5d 17 d2 a4 a4 48 3c ba
        */
    }else if(type == TYPE_FLUSH){
        ast_log(AST_LOG_DEBUG, "TYPE_FLUSH\n");
        /*
        TYPE_FLUSH
        00 66 00 e6 01 52
        */
    }else if(type == TYPE_FLUSHED){
        ast_log(AST_LOG_DEBUG, "TYPE_FLUSHED\n");
    }else{
        ast_log(AST_LOG_NOTICE, "TYPE_UNKNOWN\n");
        print_packet(AST_LOG_NOTICE, buf, len);
    }
    return 0;
}

static int admin_unpack(unsigned char *buf, int len, peer_t *svx_s){
    /*
        Format: [4 bytes Big Endian][2 bytes BE]...
                ^^^^^^^^^^^^^^^^^^^ ^^^^^^^^^^^
                |                   +- type
                +- length
    */
    uint32_t length;
    uint16_t type;
    unsigned char out[1024];
    int olen, r;
    uint16_t n;

    if(len < 6){
        return 0;
    }
    print_packet(AST_LOG_DEBUG, buf, len);
    length = UNPACK_INT(buf);
    type = UNPACK_SHORT(&buf[4]);
    if(type == TYPE_HEARTBEAT){
        ast_log(AST_LOG_DEBUG, "TYPE_HEARTBEAT\n");
    }else if(type == TYPE_PROTOVER){
        ast_log(AST_LOG_DEBUG, "TYPE_PROTOVER\n");
    }else if(type == TYPE_AUTHCHAL){
        /* siz(4) type(2) callsize(2) call(X) digsize(2) dig(20) */
        if(len < (length + 4)){
            return 0;
        }
        ast_log(AST_LOG_DEBUG, "TYPE_AUTHCHAL\n");
        olen = 30 + strlen(svx_s->callsign);
        bzero(out, 1024);
        if(compute_digest(&buf[8], svx_s->password, strlen(svx_s->password),
            &out[10+strlen(svx_s->callsign)])){
            return 0;
        }
        r = 0;
        r += pack_int(out, olen-4);
        r += pack_short(&out[r], TYPE_AUTHRESP);
        r += pack_string(&out[r], svx_s->callsign);
        r += pack_short(&out[r], 20);
        r = send(svx_s->admin_sock, out, olen, 0);
        if(r != olen){
            ast_log(AST_LOG_WARNING, "send failed?\n");
        }
    }else if(type == TYPE_AUTHRESP){
        ast_log(AST_LOG_DEBUG, "TYPE_AUTHRESP\n");
    }else if(type == TYPE_AUTHOK){
        ast_log(AST_LOG_NOTICE, "TYPE_AUTHOK\n");
        svx_s->busy = 0;
        svx_s->tx = 0;
    }else if(type == TYPE_ERROR){
        char errstring[80];
        uint16_t strl;
        int l;
        ast_log(AST_LOG_WARNING, "TYPE_ERROR\n");
        if(len < (length + 4)){
            return 0;
        }
        strl = UNPACK_SHORT(&buf[6]);
        l = (strl < 79) ? strl : 79;
        memcpy(errstring, &buf[8], l);
        errstring[l] = '\0';
        ast_log(AST_LOG_WARNING, "error: %s\n", errstring);
    }else if(type == TYPE_SERVINFO){
        if(len < (length + 4)){
            return 0;
        }
        ast_log(AST_LOG_DEBUG, "TYPE_SERVINFO\n");
        svx_s->client_id = UNPACK_INT(&buf[6]);
        ast_log(AST_LOG_NOTICE, "client_id: 0x%04x\n", svx_s->client_id);
        n = UNPACK_SHORT(&buf[10]);
        svx_s->count = 0;
        voice_send_heartbeat(svx_s);
        // TODO I don't care for nodes and codecs at the moment
        /*
        18:13:05.350065 IP 10.1.0.42.5300 > 79.3.121.70.51922: Flags [P.], seq 35:154, ack 47, win 227, options [nop,nop,TS val 3299653196 ecr 1240103235], length 119
	0x0000:  4500 00ab c961 4000 4006 9e77 0a01 002a  E....a@.@..w...*
	0x0010:  4f03 7946 14b4 cad2 b173 7f66 7d81 c418  O.yF.....s.f}...
	0x0020:  8018 00e3 f4f1 0000 0101 080a c4ac b64c  ...............L
	0x0030:  49ea 7943 0000 0073 0064 0000 000b 000c  I.yC...s.d......
	0x0040:  0006 5265 644e 6574 0006 4952 3655 4157  ..RedNet..IR6UAW
	0x0050:  0006 4957 3744 5a52 0005 4952 3755 4700  ..IW7DZR..IR7UG.
	0x0060:  0549 5233 414e 0005 4952 3641 4100 0949  .IR3AN..IR6AA..I
	0x0070:  5236 5541 452d 3730 0006 4952 3655 4455  R6UAE-70..IR6UDU
	0x0080:  0006 4957 364f 434d 0006 4952 3655 4456  ..IW6OCM..IR6UDV
	0x0090:  0009 4952 3655 4145 2d32 3300 0649 5236  ..IR6UAE-23..IR6
	0x00a0:  5543 5400 0100 044f 5055 53              UCT....OPUS
        */
    }else if(type == TYPE_NODELIST){
        ast_log(AST_LOG_DEBUG, "TYPE_NODELIST\n");
    }else if(type == TYPE_NODEJOIN){
        ast_log(AST_LOG_DEBUG, "TYPE_NODEJOIN\n");
    }else if(type == TYPE_NODELEFT){
        ast_log(AST_LOG_DEBUG, "TYPE_NODELEFT\n");
    }else if(type == TYPE_TALKSTART){
        ast_log(AST_LOG_DEBUG, "TYPE_TALKSTART\n");
        svx_s->busy = 1;
        /*
        admin_unpack: TYPE_TALKSTART
        00 00 00 0a 00 68 00 06 49 52 36 55 44 56
        00 00 00 0a 00 68 00 06 49 52 36 55 44 56
        len (4), type, len, callsign
        */
    }else if(type == TYPE_TALKSTOP){
        ast_log(AST_LOG_DEBUG, "TYPE_TALKSTOP\n");
        svx_s->busy = 0;
        /*
        TYPE_TALKSTOP
        00 00 00 0a 00 69 00 06 49 52 36 55 44 56
        */
    }else{
        ast_log(AST_LOG_NOTICE, "TYPE_UNKNOWN\n");
        print_packet(AST_LOG_NOTICE, buf, len);
    }
    if(len > (length + 4)){
        // 2 packets in 1 recv?
        return (length+4)+admin_unpack(&buf[length+4], len-length-4, svx_s);
    }
    return length+4;
}

/* ***************************************************************************/
/* ***************************************************************************/
/* ***************************************************************************/

ASTERISK_FILE_VERSION(__FILE__, "$Revision$")

static char type[] = "svxlink";
static char tdesc[] = "svxlink channel driver by DL1AYV";

static struct ast_channel *svxlink_request(const char *type,
    struct ast_format_cap *cap, const struct ast_assigned_ids *assignedids,
    const struct ast_channel *requestor, const char *addr, int *cause);
static int svxlink_call(struct ast_channel *ast, const char *dest, int timeout);
static int svxlink_hangup(struct ast_channel *ast);
static int svxlink_write(struct ast_channel *chan, struct ast_frame *f);
static int svxlink_indicate(struct ast_channel *ast, int cond, const void *data,
    size_t datalen);
static struct ast_frame *svxlink_read(struct ast_channel *chan);
static int *svxlink_dtmf_end(struct ast_channel *chan, char digit,
    unsigned int duration);

static struct ast_channel_tech svxlink_tech = {
    .type = type,
    .description = tdesc,
    .requester = svxlink_request,
    .call = svxlink_call,
    .write = svxlink_write,
    .read = svxlink_read,
    .hangup = svxlink_hangup,
    .send_digit_end = svxlink_dtmf_end,
    .indicate = svxlink_indicate
};

/* FIXME to remove
struct ast_format {
        const char *name;
        struct ast_codec *codec;
        void *attribute_data;
        const struct ast_format_interface *interface;
        unsigned int channel_count;
};
*/

static int toggle_tx(peer_t *p){
    if((!p->busy) && (!p->tx)){
        p->tx = 1;
        ast_log(AST_LOG_DEBUG, "TXing\n");
    }else{
        p->tx = 0;
        ast_log(AST_LOG_DEBUG, "RXing\n");
    }
    return (int)p->tx;
}

static int tx(peer_t *p){
    if((!p->busy) && (!p->tx)){
        p->tx = 1;
        ast_log(AST_LOG_DEBUG, "TXing\n");
    }
    return (int)p->tx;
}

static int rx(peer_t *p){
    if(p->tx){
        p->tx = 0;
        ast_log(AST_LOG_DEBUG, "RXing\n");
    }
    return (int)p->tx;
}

# define TOGGLE_CENTS_THR 100
# define UNDIAL_CENTS_THR 300

static int *svxlink_dtmf_end(struct ast_channel *chan, char digit,
    unsigned int duration){
    peer_t *p;
    ast_log(AST_LOG_DEBUG, "digit %c dur %d\n", digit, duration);
    p = ast_channel_tech_pvt(chan);
    if(p == NULL){
        return -1;
    }
    //ast_mutex_lock(&(p->lock));
    switch(digit){
        case '1':
            break;
        case '2':
            break;
        case '3':
            break;
        case '4':
            break;
        case '5':
            break;
        case '6':
            break;
        case '7':
            break;
        case '8':
            break;
        case '9':
            //if(duration > UNDIAL_CENTS_THR)
                //??
            break;
        case '0':
            if(duration > TOGGLE_CENTS_THR)
                if(toggle_tx(p)){
                    send_talk_start(p);
                }else{
                    send_audio_flush(p);
                    send_talk_stop(p);
                }
            break;
        case '*':
            if(duration > TOGGLE_CENTS_THR)
                if(tx(p)){
                    send_talk_start(p);
                }
            break;
        case '#':
            if(duration > TOGGLE_CENTS_THR){
                rx(p);
                send_audio_flush(p);
                send_talk_stop(p);
            }
            break;
    }
    //ast_mutex_unlock(&(p->lock));
    return 0;
}

static int svxlink_write(struct ast_channel *chan, struct ast_frame *f){
    peer_t *p;
    p = ast_channel_tech_pvt(chan);
    if(p == NULL){
        return 0;
    }
    //ast_mutex_lock(&(p->lock));
    if(f->frametype != AST_FRAME_VOICE){
        ast_log(AST_LOG_NOTICE, "frame type: %d\n", f->frametype);
        return 0;
    }
    if(!p->tx)
        return 0;
    send_audio_frame(p,f);
    //ast_mutex_unlock(&(p->lock));
    return 0;
}

static struct ast_frame *svxlink_read(struct ast_channel *chan){
    ast_log(AST_LOG_WARNING, "svxlink read\n");
    return NULL;
}

static int svxlink_indicate(struct ast_channel *ast, int cond, const void *data,
    size_t datalen){
    ast_log(AST_LOG_NOTICE, "indicate cond: %d\n", cond);
    return 0;
}

static peer_t *search_peer(char *req){
    int i;
    for(i = 0; i < num_peers; i++){
        if(strcmp(req, peers[i].addr) == 0){
            return &peers[i];
        }
    }
    return NULL;
}

static struct ast_channel *svxlink_new(struct peer *p, int state,
    const struct ast_assigned_ids *assignedids,
    const struct ast_channel *requestor) {
        char context[] = "default";
        struct ast_channel *tmp;

        tmp = ast_channel_alloc(0, state, NULL, NULL, "", "s", context,
            assignedids, requestor, 0, "svxlink/%s", p->addr);
        if (tmp) {
            ast_channel_tech_set(tmp, &svxlink_tech);
            ast_channel_nativeformats_set(tmp, svxlink_tech.capabilities);
            ast_channel_set_rawreadformat(tmp, ast_format_opus);
            ast_channel_set_rawwriteformat(tmp, ast_format_opus);
            ast_channel_set_writeformat(tmp, ast_format_opus);
            ast_channel_set_readformat(tmp, ast_format_opus);
            if (state == AST_STATE_RING)
                ast_channel_rings_set(tmp, 1);
            ast_channel_tech_pvt_set(tmp, p);
            ast_channel_context_set(tmp, context);
            ast_channel_exten_set(tmp, "s");
            ast_channel_language_set(tmp, "");
            //ast_mutex_lock(&(p->lock));
            p->channel = tmp;
            //ast_mutex_unlock(&(p->lock));
            //i->u = ast_module_user_add(tmp);
            ast_channel_unlock(tmp);
            if (state != AST_STATE_DOWN) {
                if (ast_pbx_start(tmp)) {
                    ast_log(AST_LOG_WARNING, "Unable to start PBX on %s\n",
                        ast_channel_name(tmp));
                    ast_hangup(tmp);
                }
            }
        } else
            ast_log(AST_LOG_WARNING, "Unable to allocate channel structure\n");
    return tmp;
}

static struct ast_channel *svxlink_request(const char *type,
    struct ast_format_cap *cap, const struct ast_assigned_ids *assignedids,
    const struct ast_channel *requestor, const char *addr, int *cause){
    peer_t* p;
    ast_log(AST_LOG_DEBUG, "request %s/%s\n", type, addr);
    p = search_peer(addr);
    if(p == NULL){
        *cause = AST_CAUSE_INVALID_CALL_REFERENCE;
        return NULL;
    }
    if(p->admin_sock == -1){
        *cause = AST_CAUSE_SUBSCRIBER_ABSENT;
        return NULL;
    }
    if(p->channel != NULL){
        *cause = AST_CAUSE_BUSY;
        return NULL;
    }
    return svxlink_new(p, AST_STATE_DOWN, assignedids, requestor);
}

static int svxlink_call(struct ast_channel *ast, const char *dest, int timeout){
    struct ast_frame f = { AST_FRAME_CONTROL };
    int cs = ast_channel_state(ast);
    peer_t *p;
    p = ast_channel_tech_pvt(ast);
    ast_log(AST_LOG_DEBUG, "call %s\n", dest);
    // iax2 checks also AST_STATE_RESERVED here .. 
    if (cs != AST_STATE_DOWN){
        ast_log(AST_LOG_WARNING, "called with status %d (%s)\n", cs,
            ast_state2str(cs));
        return -1;
    }
    f.subclass.integer = AST_CONTROL_ANSWER;
    if(p != NULL){
        f.src = p->addr;
    }
    //f.subclass.integer = AST_CONTROL_RINGING;
    ast_queue_frame(ast, &f);
    ast_setstate(ast, AST_STATE_UP);
    return 0;
}

static int svxlink_hangup(struct ast_channel *ast){
    peer_t *p;
    ast_log(AST_LOG_DEBUG, "hangup\n");
    p = ast_channel_tech_pvt(ast);
    if(p != NULL){
        //ast_mutex_lock(&(p->lock));
        p->channel = NULL;
        //ast_mutex_unlock(&(p->lock));
        if(p->tx){
            toggle_tx(p);
            send_audio_flush(p);
            send_talk_stop(p);
        }
    }
    ast_channel_tech_pvt_set(ast, NULL);
    ast_setstate(ast, AST_STATE_DOWN);
    return 0;
}

static int build_admin_chan(char *addr, in_port_t port){
    int s;
    struct hostent *host;
    struct ast_hostent ah;
    struct sockaddr_in sin;
    struct timeval tv;

    host = ast_gethostbyname(addr, &ah);
    if(!host){
        ast_log(AST_LOG_ERROR, "unknown host %s\n", addr);
        return -1;
    }

    memset((void *)&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_port = htons(port);
    memcpy(&sin.sin_addr, host->h_addr, sizeof(sin.sin_addr));
    s = socket(PF_INET, SOCK_STREAM, 0);

    tv.tv_sec = 4;
    tv.tv_usec = 0;
    if(setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv))){
        ast_log(AST_LOG_ERROR, "setsockopt failed\n");
    }

    if(connect(s, &sin, sizeof(sin)) != 0){
        ast_log(AST_LOG_ERROR, "connect() failed\n");
        return -1;
    }
    return s;
}

static int build_voice_chan(char *addr, in_port_t port){
    int s;
    struct hostent *host;
    struct ast_hostent ah;
    struct sockaddr_in sin;
    struct timeval tv;

    host = ast_gethostbyname(addr, &ah);
    if(!host){
        ast_log(AST_LOG_ERROR, "unknown host %s\n", addr);
        return -1;
    }
    memset((void *)&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_port = htons(port);
    memcpy(&sin.sin_addr, host->h_addr, sizeof(sin.sin_addr));
    s = socket(PF_INET, SOCK_DGRAM, 0);

    tv.tv_sec = 4;
    tv.tv_usec = 0;
    if(setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv))){
        ast_log(AST_LOG_ERROR, "setsockopt failed (udp)\n");
    }

    if(connect(s, &sin, sizeof(sin)) != 0){
        ast_log(AST_LOG_ERROR, "connect() failed (udp)");
        return -1;
    }
    return s;
}

static void *admin_monitor_thread(peer_t *svx_s){
    int r, n = 0, tot = 0;
    unsigned char buf[1024];
    while(1){
        r = recv(svx_s->admin_sock, &buf[tot], 1024-tot, 0);
        if((svx_s->admin_sock > 0) && (r < 0)){
            // tmout?
            if(r != -1){
                ast_log(AST_LOG_WARNING, "r: %d\n", r);
            }
            admin_send_heartbeat(svx_s->admin_sock);
        }else if((svx_s->admin_sock <= 0) || (r == 0)){
            tot = 0; n = 0;
            ast_log(AST_LOG_WARNING, "(re)connect %s\n", svx_s->addr);
            close(svx_s->admin_sock);
            svx_s->admin_sock = build_admin_chan(svx_s->addr, svx_s->port);
            if(svx_s->admin_sock == -1){
                ast_log(AST_LOG_WARNING, "network problem, sleep 10 secs\n");
                sleep(10);
                continue;
            }
            send_proto_version(svx_s->admin_sock);
        }else{
            tot += r;
            if(tot >= 1024){
                ast_log(AST_LOG_WARNING, "lost bytes, out of sync? reset..\n");
                n = tot;
            }else{
                n += admin_unpack(&buf[n], tot-n, svx_s);
            }
            if(tot == n){
                n = 0;
                tot = 0;
            }
        }
    }
    return NULL;
}

static void *voice_monitor_thread(peer_t *svx_s){
    int r;
    unsigned char buf[1024];
    while(1){
        r = recv(svx_s->voice_sock, buf, 1024, 0);
        if((svx_s->voice_sock > 0) && (r < 0)){
            // tmout?
            if(r != -1){
                ast_log(AST_LOG_WARNING, "r: %d\n", r);
            }
            voice_send_heartbeat(svx_s);
        }else if((svx_s->voice_sock <= 0) || (r == 0)){
            ast_log(AST_LOG_WARNING, "(re)opening socket .. ??\n");
                sleep(10);
            break;
        }else{
            voice_unpack(buf, r, svx_s);
        }
    }
    return NULL;
}

static int start_network_threads(void){
    int i;

    for(i = 0; i < num_peers; i++){
        peers[i].admin_sock = build_admin_chan(peers[i].addr, peers[i].port);
        peers[i].voice_sock = build_voice_chan(peers[i].addr, peers[i].port);
        ast_pthread_create_detached(&(peers[i].admin_thread), NULL,
            admin_monitor_thread, &peers[i]);
        ast_pthread_create_detached(&(peers[i].voice_thread), NULL,
            voice_monitor_thread, &peers[i]);
        send_proto_version(peers[i].admin_sock);
    }
    return 0;
}

static int generate_peers(void){
    char filename[] = "svxlink.conf";
    struct ast_config *cfg;
    char *prev = NULL;
    char *cat;
    struct ast_flags flags = { 0 };
    int i = -1;
    char *tmp;

    cfg = ast_config_load(filename, flags);
    if(cfg == NULL){
        ast_log(AST_LOG_WARNING, "no svxlink conf, module won't load\n");
        return -1;
    }
    while(1){
        cat = ast_category_browse(cfg, prev);
        if(cat == NULL)
            break;
        prev = cat;
        i += 1;
        if(i == (MAX_PEERS-1)){
            break;
        }
        ast_mutex_init(&(peers[i].lock));
        ast_mutex_lock(&(peers[i].lock));
        strncpy(peers[i].addr, cat, MAX_ADDR_LEN);
        tmp = ast_variable_retrieve(cfg, cat, "callsign");
        if(tmp == NULL){
            break;
        }
        strncpy(peers[i].callsign, tmp, MAX_CALL_LEN);
        tmp = ast_variable_retrieve(cfg, cat, "port");
        if(tmp == NULL){
            ast_mutex_unlock(&(peers[i].lock));
            break;
        }
        // FIXME check sscanf() out..
        sscanf(tmp, "%hu", &peers[i].port);
        tmp = ast_variable_retrieve(cfg, cat, "password");
        if(tmp == NULL){
            ast_mutex_unlock(&(peers[i].lock));
            break;
        }
        strncpy(peers[i].password, tmp, MAX_PASS_LEN);

        peers[i].dsp = ast_dsp_new();
        if(peers[i].dsp == NULL){
            ast_log(AST_LOG_WARNING, "could not allocate dsp for %s\n",
                peers[i].addr);
        }else{
            ast_dsp_set_features(peers[i].dsp, DSP_FEATURE_DIGIT_DETECT);
            ast_dsp_set_digitmode(peers[i].dsp,
                DSP_DIGITMODE_DTMF | DSP_DIGITMODE_RELAXDTMF);
        }
        ast_mutex_unlock(&(peers[i].lock));

        ast_log(AST_LOG_NOTICE, "loaded server: %s\n", peers[i].addr);
        ast_log(AST_LOG_NOTICE, "on port: %hu\n", peers[i].port);
        ast_log(AST_LOG_NOTICE, "with callsign: %s\n", peers[i].callsign);
    }
    ast_config_destroy(cfg);
    if(i == -1){
        return -1;
    }
    num_peers = i + 1;
    return 0;
}

static int load_module(void) {
    if(generate_peers() == -1){
        return AST_MODULE_LOAD_DECLINE;
    }
    if(start_network_threads() == -1){
        return AST_MODULE_LOAD_FAILURE;
    }
    svxlink_tech.capabilities = ast_format_cap_alloc(
        AST_FORMAT_CAP_FLAG_DEFAULT);
    if (!svxlink_tech.capabilities){
        ast_log(AST_LOG_ERROR, "ast_format_cap_alloc() failed");
        return AST_MODULE_LOAD_FAILURE;
    }
    ast_format_cap_append(svxlink_tech.capabilities, ast_format_opus, 0);

    if (ast_channel_register(&svxlink_tech)) {
        ast_log(AST_LOG_ERROR, "Unable to register channel class %s\n", type);
        // use also AST_MODULE_LOAD_DECLINE
        return AST_MODULE_LOAD_FAILURE;
    }
    ast_log(AST_LOG_NOTICE, "svxlink module loaded\n");
    return AST_MODULE_LOAD_SUCCESS;
}

static int unload_module(void) {
    //int i;
    // FIXME stop all threads and sockets before this
    //for(i = 0; i < num_peers; i++){
    //    ast_mutex_destroy(&(peers[i].lock));
    //}
    ast_channel_unregister(&svxlink_tech);
    return 0;
}


AST_MODULE_INFO_STANDARD(ASTERISK_GPL_KEY, tdesc);

